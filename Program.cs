﻿using System;


namespace StructConsole
{
    
    public struct Result
    {
        
        public string Subject; 
        public string Teacher;
        public int Point;

        public Result(string subject, string teacher, int point)
        {
            Subject = subject;
            Teacher = teacher;
            Point = point;

        }
        public Result(string subject, string teacher)
        {
            Subject = subject;
            Teacher = teacher;
            Point = -5;
        }
    }

    public struct Student
    {
        public string Name;
        public string Surname;
        public int Year;
        public string Group;
        public Result[] Results;

        public Student(string name, string surname, int year, string group, Result[] results)
        {
            Name = name;
            Surname = surname;
            Year = year;
            Group = group;
            Results = results;
        }

        public Student(string name, string surname, int year, Result[] results)
        {
            Name = name;
            Surname = surname;
            Year = year;
            Group = "Unknown";
            Results = results;
        }
        // --------- Средние баллы ---------
        public double GetAveragePoints()
        {
            int sum = 0;
            foreach (Result result in Results)
            {
                sum += result.Point;
            }
            sum /= Results.Length;
            return sum;
        }
        // --------- Получить лучший предмет ---------
        public string GetBestSubject()
        {
            Result bestResult;
            bestResult = Results[0];
            foreach (Result result in Results)
            {
                if (result.Point > bestResult.Point)
                {
                    bestResult = result;
                }
            }
            return bestResult.Subject;
        }
        // --------- Получить худший предмет ---------
        public string GetWorstSubject()
        {
            Result worstResult;
            worstResult = Results[0];
            foreach (Result result in Results)
            {
                if (result.Point < worstResult.Point)
                {
                    worstResult = result;
                }
            }
            return worstResult.Subject;
        }
    }

    class Program
    {
        
        // ======== Проверить число на целостность ========
        static void CheckInt(out int x, int min = 0, int max = 100)
        {
            bool ok;
            do
            {
                ok = int.TryParse(Console.ReadLine(), out x);
                if (!ok || x < min || x > max)
                    Console.WriteLine("Некоректне введення! Повторіть спробу.");
            } while (!ok || x < min || x > max);
        }
        // ======== Получить число ========

        static int GetValue()
        {
            int n; bool isRight = false;
            do
            {
                isRight = int.TryParse(Console.ReadLine(), out n);
                if (n <= 0) isRight = false;
            } while (!isRight);
            return n;
        }
        // ======== Получить средний бал ========


        // ======== Вывести студентов ======== 
        static void PrintStudents(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine($"\n-----Студент {i + 1}-----");
                PrintStudent(students[i]);
            }
        }
        // ======== Показать меню ========
        static void ShowMenu()
        {

            Console.WriteLine("" +
                "\n1-  |Створити запис                      |" +
                "\n2 - |Інформація про всіх студентів       |" +
                "\n3 - |Інформація про конкретного студента |" +
                "\n4 - |Самий високий і низький бал         |" +
                "\n5 - |Сортування від А до Я               |" + 
                "\n6 - |Сортувати по середньому балу        |" +
                "\n0 - |Вихід                               |");


        }
        // ======== Вывод полного студента ========

        static void PrintStudent(Student student)
        {
            Console.WriteLine($"Имя: {student.Name}");
            Console.WriteLine($"Фамилия: {student.Surname}");
            Console.WriteLine($"Шифр группы: {student.Group}");
            Console.WriteLine($"Курс: {student.Year}");

            for (int k = 0; k < student.Results.Length; k++)
            {
                Console.WriteLine($"Предмет: {student.Results[k].Subject}");
                Console.WriteLine($"Викладач: {student.Results[k].Teacher}");
                Console.WriteLine($"Оцінка: {student.Results[k].Point}");
            }
            Console.WriteLine($"Предмет з найкращою успішністю: {student.GetBestSubject()}");
            Console.WriteLine($"Предмет з найгіршою успішністюю: {student.GetWorstSubject()}");
        }



        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Default;

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.SetWindowSize(100, 25);
            Console.Title = "Лабораторна робота  №5 Верещинський Артем КН-20-2";
            // Начало меню
          //  Student[] students = null;
            int select;
             Student Nikolay = new Student("Артем", "Володимирович", 5, "КН-20-2", new Result[2] { new Result("Математика", "КМР", 55), new Result("УкрМова", "РУГ", 100) });
            Student Vasyliy = new Student("Олексій", "Волосевич", 5, "КН-20-2", new Result[2] { new Result("Математика", "КМР", 78), new Result("УкрМова", "РУГ", 96) });

            // Проверочный студент
            Student[] students = {Vasyliy, Nikolay };
            do
            {
                ShowMenu();
                CheckInt(out select, 0, 50);
                switch (select)
                {

                    case 1:
                        students = ReadResultArray(ref students);
                        break;

                    case 2:
                        if (students == null)
                        {
                            Console.WriteLine("Добавьте запись к структуре\n");
                            break;
                        }
                        else
                        {
                            PrintStudents(students);
                        }
                        break;

                    case 3:
                        if (students == null)
                        {
                            Console.WriteLine("Додайте запис до структури\n");
                            break;
                        }
                        Console.WriteLine($"Введіть номер студента(1 ; {students.Length})?");
                        int num = 0;
                        CheckInt(out num, 0, students.Length);
                        PrintStudent(students[num - 1]);
                        break;

                    case 4:
                        if (students == null)
                        {
                            Console.WriteLine("Додайте запис до структури\n");
                            break;
                        }
                        else
                        {
                            double maxAverage, minAverage;
                            GetStudentsInfo(students, out maxAverage, out minAverage);
                        }
                        break;

                    case 5:

                        if (students == null)
                        {
                            Console.WriteLine("Додайте запис до структури\n");
                            break;
                        }
                        else
                        {
                            Array.Sort(students, InfoStudentsByName);
                            PrintStudents(students);
                        }
                        break;

                    case 6:
                        if (students == null)
                        {
                            Console.WriteLine("Додайте запис до структури\n");
                            break;
                        }
                        else
                        {
                            Array.Sort(students, InfoByAveragePoints);
                            PrintStudents(students);
                        }
                        break;
                    case 0:
                        break;

                    default:

                        Console.WriteLine("\nВиберіть будь-ласка щось із даного списку ");

                        break;
                }
            } while (select != 0);
            // Конец меню, пока select != 0

        }
        //  ------------- Получить информацию о студенте
        static void GetStudentsInfo(Student[] students, out double imaxAverage, out double iminAverage)
        {
            double min = 100, max = 0;
            int iMax = 0, iMin = 0;

            for (int i = 0; i < students.Length; i++)
            {
                double temp = students[i].GetAveragePoints();

                if (temp > max)
                {
                    max = temp;
                    iMax = i;
                }
                if (temp < min)
                {
                    min = temp;
                    iMin = i;
                }
            }
            imaxAverage = iMax;
            iminAverage = iMin;
            Console.WriteLine($"\nМаксимальний бал: {max:F2}");
            Console.WriteLine($"Мінімальный бал: {min:F2}\n");
        }
        // ------------- Ввод данных в структуру -------------
        public static Student[] ReadResultArray(ref Student[] students)
        {
           
            int year = 0;
            int n;
            Console.WriteLine($"\nВведіть кількість студентів n <=100: ");
            CheckInt(out n);
            students = new Student[n];
            
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"\n______{i + 1}___-ий студент______");
                Console.Write("\nВведіть Ім'я: ");
                students[i].Name = Console.ReadLine();
                Console.Write("\nВведіть Фамілію: ");
                students[i].Surname = Console.ReadLine();
                Console.Write("\nВведіть шифр групи: ");
                students[i].Group = Console.ReadLine();
                Console.WriteLine("Введіть курс: ");
                CheckInt(out year, 1, 4);
                students[i].Year = year;

                Console.WriteLine("Введіть к-сть результатів (1-10): ");
                int number; // Кол-во предметов
                CheckInt(out number, 1, 10);
                /*while (!input) // Удалить перед сдачей
                {
                    if (int.TryParse(Console.ReadLine(), out year))
                        input = true;
                    else
                        Console.WriteLine("Ошибка, попробуйте снова!");
                } */
                students[i].Results = new Result[year];
                for (int j = 0; j < number; j++)
                {
                    Console.WriteLine($"\n{j + 1}");
                    Console.Write("Назва предмета: ");
                    students[i].Results[j].Subject = Console.ReadLine();
                    Console.Write("ФИО викладача: ");
                    students[i].Results[j].Teacher = Console.ReadLine();
                    int pointstud;
                    Console.Write("Оцінка від 1 до 100: ");
                    CheckInt(out pointstud, 1, 100);
                    students[i].Results[j].Point = pointstud;
                }
                Console.WriteLine("\n");
            }
            return students;
        }
        // +++++++++++++++  Сортировка +++++++++++++
        public static int InfoStudentsByName(Student a, Student b)
        {
            string nameA = a.Surname;
            string nameB = b.Surname;
            for (int i = 0; i < (nameA.Length < nameB.Length ? nameA : nameB).Length; i++)
            {
                if ((int)nameA[i] > (int)nameB[i])
                {
                    return 1;
                }

                if ((int)nameA[i] < (int)nameB[i])
                {
                    return -1;
                }
            }

            nameA = a.Name;
            nameB = b.Name;
            for (int i = 0; i < (nameA.Length < nameB.Length ? nameA : nameB).Length; i++)
            {
                if ((int)nameA[i] > (int)nameB[i])
                {
                    return 1;
                }

                if ((int)nameA[i] < (int)nameB[i])
                {
                    return -1;
                }
            }
            return 0;
        }

        // ------------ Сортировка по баллам ------------
        public static int InfoByAveragePoints(Student a, Student b)
        {
            double avgA = a.GetAveragePoints(), avgB = b.GetAveragePoints();
            if (avgA > avgB)
            {
                return 1;
            }
            if (avgA < avgB)
            {
                return -1;
            }
            return 0;
        }
    }
}
